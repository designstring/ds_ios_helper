//
//  DSColors.swift
//  Lemoney
//
//  Created by Punith B M on 02/10/17.
//  Copyright © 2017 Lemoney. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

  static let primaryColor = UIColor(hex: "4ABEFF")
  static let secondayColor = UIColor(hex: "4ABEFF")
  static let transparentBlack = UIColor(white: 0, alpha: 0.5)

    convenience init(hex: String) {
      let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
      var int = UInt32( )
      Scanner(string: hex).scanHexInt32(&int)
      let a, r, g, b: UInt32
      switch hex.count {
      case 3: // RGB (12-bit)
        (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
      case 6: // RGB (24-bit)
        (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
      case 8: // ARGB (32-bit)
        (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
      default:
        (a, r, g, b) = (255, 0, 0, 0)
      }
      self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }

  open func randomColor() -> UIColor {
    srandom(arc4random())
    var red: Float = 0
    while (red < 0.1 || red > 0.84) {
      red = Float(drand48())
    }
    var green: Float = 0
    while (green < 0.1 || green > 0.84) {
      green = Float(drand48())
    }
    var blue: Float = 0
    while (blue < 0.1 || blue > 0.84) {
      blue = Float(drand48())
    }
    return .init(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: 1.0)
  }

}
