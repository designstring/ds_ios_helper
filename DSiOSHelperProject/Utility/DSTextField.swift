//
//  DSTextField.swift
//  Lemoney
//
//  Created by Ruthwick S Rai on 09/04/18.
//  Copyright © 2018 Lemoney. All rights reserved.
//

import Foundation
import UIKit

class DSTextField: UITextField {
  override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    if action == #selector(copy(_:)) || action == #selector(paste(_:)) {
      return false
    }
    
    return true
  }

}
