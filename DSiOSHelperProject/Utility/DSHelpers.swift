//
//  DSHelpers.swift
//  Lemoney
//  Created by Punith B M on 19/10/17.
//  Copyright © 2017 Lemoney. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation
import CoreTelephony


public var availableSIM: Bool {
  return CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode != nil
}

public func jailBroken()-> Bool{
  if TARGET_IPHONE_SIMULATOR != 1
  {
    if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
      || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
      || FileManager.default.fileExists(atPath: "/bin/bash")
      || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
      || FileManager.default.fileExists(atPath: "/etc/apt")
      || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
    {
      return true
    }
  }
  return false
}

public func clearALL(){
  
}

public func getDateString(_ date: String) -> String {
  let dateFormatter: DateFormatter = DateFormatter()
  dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
  let dateFormatted = dateFormatter.date(from: date)
  dateFormatter.dateFormat = "yyyy-MM-dd"
  return  dateFormatter.string(from: dateFormatted!)
}

public func getMonthString(_ date: String) -> String {
  let dateFormatter: DateFormatter = DateFormatter()
  dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
  let dateFormatted = dateFormatter.date(from: date)
  dateFormatter.dateFormat = "MMMM yyyy"
  return  dateFormatter.string(from: dateFormatted!)
}

public func formattedDateString(_ date: String) -> String {
  if date == "" { return "" }
  let dateFormatter: DateFormatter = DateFormatter()
  dateFormatter.dateFormat = "yyyy-MM-dd"
  let dateFormatted = dateFormatter.date(from: date)
  let calendar = NSCalendar.current
  if calendar.isDateInYesterday(dateFormatted!) { return "Yesterday" } else if calendar.isDateInToday(dateFormatted!) { return "Today" }
  dateFormatter.dateFormat = "EEE, dd MMM"
  return  dateFormatter.string(from: dateFormatted!)
}

public func formattedDateTimeString(_ date: String) -> String {
  if date == "" { return "" }
  let dateFormatter: DateFormatter = DateFormatter()
  dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
  let dateFormatted = dateFormatter.date(from: date)
  let calendar = NSCalendar.current
  if calendar.isDateInYesterday(dateFormatted ?? Date()) { return "Yesterday" } else if calendar.isDateInToday(dateFormatted ?? Date()) {
    dateFormatter.dateFormat = "h:mm a"
    dateFormatter.amSymbol = "AM"
    dateFormatter.pmSymbol = "PM"
    return  dateFormatter.string(from: dateFormatted ?? Date())
  }
  dateFormatter.dateFormat = "EEE,dd MMM"
  return  dateFormatter.string(from: dateFormatted ?? Date())
}



public func formattedDateWithTimeString(_ date: String) -> String {
  if date == "" { return "" }
  let dateFormatter: DateFormatter = DateFormatter()
  dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
  let dateFormatted = dateFormatter.date(from: date)
  let calendar = NSCalendar.current
  if calendar.isDateInYesterday(dateFormatted!) {
    dateFormatter.amSymbol = "AM"
    dateFormatter.pmSymbol = "PM"
    dateFormatter.dateFormat = "h:mm a"
    let formattedDate =  dateFormatter.string(from: dateFormatted!)
    return "Yesterday,\(formattedDate)"
  } else if calendar.isDateInToday(dateFormatted!) {
    dateFormatter.amSymbol = "AM"
    dateFormatter.pmSymbol = "PM"
    dateFormatter.dateFormat = "h:mm a"
    let formattedDate =  dateFormatter.string(from: dateFormatted!)
    return "Today, \(formattedDate)"
  }
  dateFormatter.dateFormat = "EEE, dd MMM"
  return  dateFormatter.string(from: dateFormatted!)
}

public func getTime(_ date: String) -> String {
  if date.isEmpty { return "" }
  let dateFormatter: DateFormatter = DateFormatter()
  dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
  let dateFormatted = dateFormatter.date(from: date)
  dateFormatter.dateFormat = "h:mm a"
  return  dateFormatter.string(from: dateFormatted!)
}

public func DSLog(_ items: Any..., separator: String = "", terminator: String = "\n") {
  #if DEBUG
    print(items, separator:separator, terminator: terminator)
  #endif
}

func delay(_ delay: Double, closure:@escaping () -> Void) {
  let when = DispatchTime.now() + delay
  DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

public func randomString(length: Int) -> String {
  let charSet = "abcdefghijklmnopqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  var c = charSet.map { String($0) }
  var rndString: String = ""
  for _ in (1...length) {
    rndString.append(c[Int(arc4random()) % c.count])
  }
  return rndString
}


extension Bundle {
  var versionNumber: String {
    return infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.00"
  }
  var buildNumber: String {
    return infoDictionary?["CFBundleVersion"] as? String ?? "000"
  }
  var displayName: String {
    return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? ""
  }
}

public func heightForView(text: String, font: UIFont, width: CGFloat) -> CGFloat {
  let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
  label.numberOfLines = 0
  label.lineBreakMode = NSLineBreakMode.byWordWrapping
  label.font = font
  label.text = text
  label.sizeToFit()
  return label.frame.height
}

public func shake(view:Any) {
  let animation = CABasicAnimation(keyPath: "position")
  animation.duration = 0.07
  animation.repeatCount = 2
  animation.autoreverses = true
  if  ((view as? UITextField) != nil) {
    animation.fromValue = NSValue(cgPoint: CGPoint(x: (view as! UITextField).center.x - 10, y:  (view as! UITextField).center.y))
    animation.toValue = NSValue(cgPoint: CGPoint(x: (view as! UITextField).center.x + 10, y:  (view as! UITextField).center.y))
    (view as! UITextField).layer.add(animation, forKey: "position")
  } else if ((view as? UILabel) != nil) {
    animation.fromValue = NSValue(cgPoint: CGPoint(x: (view as! UILabel).center.x - 10, y:  (view as! UILabel).center.y))
    animation.toValue = NSValue(cgPoint: CGPoint(x: (view as! UILabel).center.x + 10, y:  (view as! UILabel).center.y))
    (view as! UILabel).layer.add(animation, forKey: "position")
  } else if ((view as? UICollectionView) != nil) {
    animation.fromValue = NSValue(cgPoint: CGPoint(x: (view as! UICollectionView).center.x - 10, y:  (view as! UICollectionView).center.y))
    animation.toValue = NSValue(cgPoint: CGPoint(x: (view as! UICollectionView).center.x + 10, y:  (view as! UICollectionView).center.y))
    (view as! UICollectionView).layer.add(animation, forKey: "position")
  } else if ((view as? UIView) != nil) {
    animation.fromValue = NSValue(cgPoint: CGPoint(x: (view as! UIView).center.x - 10, y:  (view as! UIView).center.y))
    animation.toValue = NSValue(cgPoint: CGPoint(x: (view as! UIView).center.x + 10, y:  (view as! UIView).center.y))
    (view as! UIView).layer.add(animation, forKey: "position")
  }
}

public func randomInt(min: Int, max: Int) -> Int {
  return min + Int(arc4random_uniform(UInt32(max - min + 1)))
}

public func checkRemoteNotificationStatus() ->Bool {
  if UIDevice.current.isSimulator {
    DSLog("checkRemoteNotificationStatus isSimulator")
    DSPrefManager.shared.setAPNS(token: randomString(length: 10))
    return true
  }
  
  let notificationType = UIApplication.shared.currentUserNotificationSettings?.types
  if notificationType?.rawValue == 0 {
    return false
  } else {
    return true
  }
}



public func showDefalutAlert(_ title: String?, _ message: String?, _ action: UIAlertAction){
  let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
  alertController.addAction(action)
  let currentTopVC: UIViewController = currentTopViewController()
  currentTopVC.present(alertController, animated: true, completion: nil)
}

extension UIApplication {
  class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
    if let navigationController = controller as? UINavigationController {
      return topViewController(controller: navigationController.visibleViewController)
    }
    if let tabController = controller as? UITabBarController {
      if let selected = tabController.selectedViewController {
        return topViewController(controller: selected)
      }
    }
    if let presented = controller?.presentedViewController {
      return topViewController(controller: presented)
    }
    return controller
  }
}

extension UIViewController {
  func performSegueToReturnBack() {
    if let nav = self.navigationController {
      nav.popViewController(animated: true)
    } else {
      self.dismiss(animated: true, completion: nil)
    }
  }
  
  func performSegueToRootView() {
    if let nav = self.navigationController {
      nav.popToRootViewController(animated: true
      )
    } else {
      self.dismiss(animated: true, completion: nil)
    }
  }
  
}

extension UITextView {
  func centerVertically() {
    let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
    let size = sizeThatFits(fittingSize)
    let topOffset = (bounds.size.height - size.height * zoomScale) / 2
    let positiveTopOffset = max(1, topOffset)
    contentOffset.y = -positiveTopOffset
  }
}

extension UIDevice {
  var isSimulator: Bool {
    #if arch(i386) || arch(x86_64)
      return true
    #else
      return false
    #endif
  }
}

extension Dictionary {
  mutating func merge(dict: [Key: Value]) {
    for (k, v) in dict {
      updateValue(v, forKey: k)
    }
  }
  mutating func update(dict:Dictionary)->[String:Any] {
    for (k, v) in dict {
      self.updateValue(v, forKey: k)
    }
    return self as! [String : Any]
  }
}

extension NotificationCenter {
  func setLMObserver(_ observer: AnyObject, selector: Selector, name: NSNotification.Name, object: AnyObject?) {
    NotificationCenter.default.removeObserver(observer, name: name, object: object)
    NotificationCenter.default.addObserver(observer, selector: selector, name: name, object: object)
  }
}


extension Data
{
  func toString() -> String
  {
    return String(data: self, encoding: .utf8) ?? ""
  }
}




// MARK: String Helper

extension StringProtocol {
  var firstUppercased: String {
    guard let first = first else { return "" }
    return String(first).uppercased() + dropFirst()
  }
}


extension String {
  
  func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
    
    return ceil(boundingBox.height)
  }
  
  func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
    return ceil(boundingBox.width)
  }
  
  func alphaNumeric() -> String {
    let allowedSet : Set<Character> =
      Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890.,")
    return String(filter {allowedSet.contains($0) })
  }
  
  
  func shortString() -> String {
    var finalString = String()
    var words = components(separatedBy: .whitespacesAndNewlines)
    
    if let firstCharacter = words.first?.first {
      finalString.append(String(firstCharacter))
      words.removeFirst()
    }
    
    if let lastCharacter = words.last?.first {
      finalString.append(String(lastCharacter))
    }
    
    return finalString.uppercased()
  }
  
  var byWords:[String] {
    var byWords:[String] = []
    enumerateSubstrings(in: startIndex..<endIndex, options: .byWords) {
      guard let word = $0 else { return }
      DSLog($1, $2, $3)
      byWords.append(word)
    }
    return byWords
  }
  
  func firstWords(_ max: Int) -> [String] {
    return Array(byWords.prefix(max))
  }
  var firstWord: String {
    return byWords.first ?? ""
  }
  func lastWords(_ max: Int) -> [String] {
    return Array(byWords.suffix(max))
  }
  var lastWord: String {
    return byWords.last ?? ""
  }
  
  var isNumeric: Bool {
    guard self.count > 0 else { return false }
    let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    return Set(self).isSubset(of: nums)
  }
  
  func encodeUrl() -> String {
    return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""
  }
  
  func domain() -> String {
    return URL(string: self)?.host ?? ""
  }

}

extension URL {
  
  public var queryParameters: [String: String]? {
    guard let components = URLComponents(url: self, resolvingAgainstBaseURL: true), let queryItems = components.queryItems else {
      return nil
    }
    
    var parameters = [String: String]()
    for item in queryItems {
      parameters[item.name] = item.value
    }
    
    return parameters
  }
}

public func currentTopViewController() -> UIViewController {
  var topController: UIViewController = (UIApplication.shared.keyWindow!.rootViewController)!
  while ((topController.presentedViewController) != nil) {
    topController = topController.presentedViewController!
  }
  return topController
}


public func getBackArrow()->UIImage {
  return UIImage.init(named: "back_arrow") ?? UIImage.init()
}

public func getBackRightArrow()->UIImage {
  return UIImage.init(named: "right_arrow") ?? UIImage.init()
}



extension UINavigationController {
  func setTransparentBar(iswhite: Bool) {
    self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    self.navigationBar.shadowImage = UIImage()
    self.navigationBar.isTranslucent = true
    self.view.backgroundColor = UIColor.white
    if iswhite {
    self.navigationBar.tintColor = UIColor.white
    self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white,NSAttributedStringKey.font: UIFont.regularFont(size: 20)]
    UIApplication.shared.statusBarStyle = .lightContent
    }else {
      self.navigationBar.tintColor = UIColor.black
      self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black,NSAttributedStringKey.font: UIFont.regularFont(size: 20)]
      UIApplication.shared.statusBarStyle = .default
    }
  }
  
  func resetNav() {
    self.navigationBar.setBackgroundImage(UINavigationBar.appearance().backgroundImage(for: UIBarMetrics.default), for:UIBarMetrics.default)
    self.navigationBar.shadowImage = UINavigationBar.appearance().shadowImage
    self.navigationBar.isTranslucent = false
    self.view.backgroundColor = UIColor.white
    self.navigationBar.tintColor = UIColor.black
    self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black,NSAttributedStringKey.font: UIFont.regularFont(size: 20)]
    UIApplication.shared.statusBarStyle = .default
  }
}


