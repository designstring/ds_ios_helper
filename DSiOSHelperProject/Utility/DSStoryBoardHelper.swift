//
//  DSStoryBoardHelper.swift
//  Lemoney
//
//  Created by Punith B M on 11/01/18.
//  Copyright © 2018 Lemoney. All rights reserved.
//

import UIKit

enum AppStoryboard : String {
  
  case DSMain
  
  var instance : UIStoryboard {
    return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
  }
  
  func viewController<T : UIViewController>(viewControllerClass : T.Type) -> T {
    let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
    return instance.instantiateViewController(withIdentifier: storyboardID) as! T
  }
  
  func initialViewController() -> UIViewController? {
    return instance.instantiateInitialViewController()
  }
}

extension UIViewController {
  
  class var storyboardID : String {
    return "\(self)"
  }
  
  static func instantiate(fromAppStoryboard appStoryboard : AppStoryboard) -> Self {
    return appStoryboard.viewController(viewControllerClass: self)
  }
  
}

