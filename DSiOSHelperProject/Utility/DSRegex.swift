//
//  DSRegex.swift
//  Lemoney
//
//  Created by Punith B M on 22/04/18.
//  Copyright © 2018 Lemoney. All rights reserved.
//

import Foundation
let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

extension String {
  func isValidEmail() -> Bool {
    if (self.count == 0 || self.range(of:"..") != nil) { return false }
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: self)
  }
  
  func checkTextCaptial() -> Bool{
    if (self.count == 0) { return false }
    let capitalLetterRegEx  = ".*[A-Z]+.*"
    let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
    let capitalresult = texttest.evaluate(with: self)
    return capitalresult
  }
  
}

