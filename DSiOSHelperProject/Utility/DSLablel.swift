//
//  DSLablel.swift
//  Lemoney
//
//  Created by Punith B M on 02/10/17.
//  Copyright © 2017 Lemoney. All rights reserved.
//

import UIKit
@IBDesignable
open class DSLabel: UILabel {

  required public init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    self.configureLabel()

  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    self.configureLabel()
  }

  open override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    configureLabel()
  }

  func configureLabel() {

//    self.textColor = UIColor.primaryColor
  }
  @IBInspectable
  public var regularFont: CGFloat = 17 {
    didSet {
      self.font = UIFont.regularFont(size: regularFont)
    }
  }

}

extension UIView {

  func startBlink(_ duration:TimeInterval) {
    UIView.animate(withDuration: 0.8,
                   delay:0.0,
                   options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                   animations: { self.alpha = 0 },
                   completion: nil)
  }

  func stopBlink() {
    layer.removeAllAnimations()
    alpha = 1
  }
}
