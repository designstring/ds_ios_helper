//
//  File.swift
//  Lemoney
//
//  Created by Punith B M on 21/03/18.
//  Copyright © 2018 Lemoney. All rights reserved.
//

import UIKit
import Security

let kSecClassValue = NSString(format: kSecClass)
let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
let kSecValueDataValue = NSString(format: kSecValueData)
let kSecClassGenericPasswordValue = NSString(format: kSecClassInternetPassword)
let kSecAttrServiceValue = NSString(format: kSecAttrServer)
let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
let kSecReturnDataValue = NSString(format: kSecReturnData)
let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)

public class DSKeychainService: NSObject {

  class func removeData(_ keyName:String) {
    let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: keyName)
    if removeSuccessful {
      DSLog("removeData success")
    } else {
      DSLog("removeData failed")
    }
  }

  class func saveData(_ data: String, _ keyName:String) {
    if KeychainWrapper.standard.hasValue(forKey: keyName){
      self.removeData(keyName)
    }
    let saveSuccessful: Bool = KeychainWrapper.standard.set(data, forKey: keyName)
    if saveSuccessful {
      DSLog("saveData success")
    } else {
      DSLog("saveData failed")
    }
  }
  
  class func loadData(_ keyName:String) -> String {
    DSLog("print",KeychainWrapper.standard.hasValue(forKey: keyName))
    return KeychainWrapper.standard.string(forKey: keyName) ?? ""
  }
}






