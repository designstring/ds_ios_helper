//
//  DSFonts.swift
//  Lemoney
//
//  Created by Punith B M on 02/10/17.
//  Copyright © 2017 Lemoney. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
  class func regularFont(size: CGFloat) -> UIFont {
    return UIFont(name: "WhitneyHTF-Medium", size: size)!
  }
  class func lightFont(size: CGFloat) -> UIFont {
    return UIFont(name: "WhitneyHTF-Light", size: size)!
  }
 class func boldFont(size: CGFloat) -> UIFont {
    return UIFont(name: "WhitneyHTF-Bold", size: size)!
  }
 class func semiBoldFont(size: CGFloat) -> UIFont {
    return UIFont(name: "WhitneyHTF-semiBold", size: size)!
  }
  
  class func bookFont(size: CGFloat) -> UIFont {
    return UIFont(name: "WhitneyHTF-Book", size: size)!
  }
  
  func sizeOfString (string: String, constrainedToWidth width: Double) -> CGSize {
    return NSString(string: string).boundingRect(
      with: CGSize(width: width, height: .greatestFiniteMagnitude),
      options: .usesLineFragmentOrigin,
      attributes: [.font: self],
      context: nil).size
  }
  
  func heightOfString (string: String, constrainedToHeight height: Double) -> CGSize {
    return NSString(string: string).boundingRect(
      with: CGSize(width: .greatestFiniteMagnitude, height: height),
      options: .usesLineFragmentOrigin,
      attributes: [.font: self],
      context: nil).size
  }
  
}


