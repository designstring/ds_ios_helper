//
//  DSPrefManager.swift
//  Lemoney
//
//  Created by Punith B M on 05/10/17.
//  Copyright © 2017 Lemoney. All rights reserved.
//

import UIKit
import CoreLocation
import Contacts
import LocalAuthentication

class DSPrefManager {
  static let shared = DSPrefManager()
  let userDefaults = UserDefaults.standard
  
  func setTimeOutValue(connectionTimeOut: String) {
    userDefaults.set(connectionTimeOut, forKey: "conn_timeout")
  }
  
  func getTimeoutValue() -> Int {
    if self.keyExits(key: "conn_timeout") {
      let timeout: String = userDefaults.string(forKey: "conn_timeout")!
      if (timeout.count > 0) {
        return Int(timeout)!
      }
    }
    return 30
  }

  func keyExits(key: String) -> Bool {
    return userDefaults.object(forKey: key) != nil
  }
  
  func setAPNS(token: String) {
    userDefaults.set(token, forKey: "apns")
  }
  
  func getAPNStoken() -> String {
    if self.keyExits(key: "apns") {
      return userDefaults.string(forKey: "apns")!
    }
    return ""
  }
  
  func setFcmToken(token: String) {
    userDefaults.set(token, forKey: "fcm_token")
  }
  
  func getFcmToken() -> String {
    if self.keyExits(key: "fcm_token") {
      return userDefaults.string(forKey: "fcm_token")!
    }
    return randomString(length: 10)
  }


}
