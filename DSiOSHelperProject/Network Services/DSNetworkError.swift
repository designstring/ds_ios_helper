//
//  DSNetworkError.swift
//  Lemoney
//
//  Created by Punith B M on 04/02/18.
//  Copyright © 2018 Lemoney. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class DSNetworkError: UIView {
  static let shared = UINib(nibName: "DSNetworkError", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DSNetworkError
  @IBOutlet var astronutImg: UIImageView!
  @IBOutlet var errorImg: UIImageView!
  @IBOutlet var errorTitle: DSLabel!
  @IBOutlet var errorDescp: UILabel!
  @IBOutlet var tryAgainBtn: UIButton!
  @IBOutlet var cancelButton: UIButton!
  @IBOutlet var tryAgainSingleBtn: UIButton!
  var currentViewController = UIViewController()
  var completionHandler: (_ responseObject: JSON, _ error: Error?)->Void = {_,_ in }
  var body = [String:Any]()
  var retry = 0
  
  class func instanceFromNib() -> DSNetworkError {
    return UINib(nibName: "DSNetworkError", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DSNetworkError
  }
  
  func loadView(){
    self.tryAgainBtn.layer.cornerRadius = 5
    let shadowPath = UIBezierPath(roundedRect: self.tryAgainBtn.bounds, cornerRadius: 5)
    let shadowPath2 = UIBezierPath(roundedRect: self.cancelButton.bounds, cornerRadius: 5)
    let shadowPath3 = UIBezierPath(roundedRect: self.tryAgainSingleBtn.bounds, cornerRadius: 5)
    self.tryAgainBtn.layer.masksToBounds = false
    self.tryAgainBtn.layer.shadowColor = UIColor.black.cgColor
    self.tryAgainBtn.layer.shadowOffset = CGSize(width: 0, height: 2)
    self.tryAgainBtn.layer.shadowOpacity = 0.2
    self.tryAgainBtn.layer.shadowPath = shadowPath.cgPath
    self.tryAgainBtn.setTitle("Try again", for: .normal)
    self.tryAgainBtn.isUserInteractionEnabled = true
    self.tryAgainSingleBtn.layer.cornerRadius = 5
    self.tryAgainSingleBtn.layer.masksToBounds = false
    self.tryAgainSingleBtn.layer.shadowColor = UIColor.black.cgColor
    self.tryAgainSingleBtn.layer.shadowOffset = CGSize(width: 0, height: 2)
    self.tryAgainSingleBtn.layer.shadowOpacity = 0.2
    self.tryAgainSingleBtn.layer.shadowPath = shadowPath3.cgPath
    self.tryAgainSingleBtn.setTitle("Try again", for: .normal)
    self.tryAgainSingleBtn.isUserInteractionEnabled = true
    self.cancelButton.layer.cornerRadius = 5
    self.cancelButton.layer.masksToBounds = false
    self.cancelButton.layer.shadowColor = UIColor.black.cgColor
    self.cancelButton.layer.shadowOffset = CGSize(width: 0, height: 2)
    self.cancelButton.layer.shadowOpacity = 0.2
    self.cancelButton.layer.shadowPath = shadowPath2.cgPath
    self.cancelButton.layer.borderColor = UIColor.init(hex: "00A4FF").cgColor
    self.cancelButton.layer.borderWidth = 1.0
    self.cancelButton.isUserInteractionEnabled = true
  }
  
  
  func loadErrorData(errorType:Int){
    self.endEditing(true)
    if errorType == 0 {
      self.astronutImg.isHidden = false
      self.tryAgainSingleBtn.isHidden = false
      self.errorImg.isHidden = true
      self.errorTitle.text = "No internet?"
      self.errorDescp.text = "We have lost contact with you. Connect to mobile data or WiFi and try again."
      self.tryAgainSingleBtn.setTitle("Try again", for: .normal)
    }else {
      self.astronutImg.isHidden = true
      self.tryAgainSingleBtn.isHidden = false
      self.errorImg.isHidden = false
      self.errorDescp.text = "We are having trouble connecting at the moment. Please try again later."
      self.tryAgainSingleBtn.isHidden = true
    }
    self.tryAgainBtn.isUserInteractionEnabled = true
    self.tryAgainSingleBtn.isUserInteractionEnabled = true
  }
  @IBAction func cancelClicked(_ sender: UIButton){
    self.isHidden = true
    DSLog("cancel clicked!")
  }
  
  
  @IBAction func tryAgainClicked(_ sender: UIButton) {
    DSLog("Tryagain clicked")
    self.tryAgainBtn.isUserInteractionEnabled = false
    self.tryAgainSingleBtn.isUserInteractionEnabled = false
    let isNetworkAvailable = DSNetworkManager.shared.isNetworkAvailable()
    if isNetworkAvailable {
      self.isHidden = true
      DSLog("currentViewController: \(currentViewController)")
      DSLog("retry: \(retry)")
      self.currentViewController.viewDidLoad()
    }else {
      DSLog("no internet")
      self.tryAgainBtn.isUserInteractionEnabled = true
      self.tryAgainSingleBtn.isUserInteractionEnabled = true
      self.isHidden = false
    }
    
  }
  
}
