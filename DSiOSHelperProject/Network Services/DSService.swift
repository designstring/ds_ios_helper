//
//  HttpUtils.swift
//  Lemoney
//
//  Created by Punith B M on 04/01/18.
//  Copyright © 2018 Lemoney. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire


struct DSService {
  static let shared = DSService()
  let appDelegate = UIApplication.shared.delegate as! DSAppDelegate
  let mPrefmanager = DSPrefManager()
  
  func request(targetViewController: UIViewController, body: [String:Any], completionHandler: @escaping (_ responseObject: JSON, _ error: Error?) -> ()){

    DSNetworkError.shared.currentViewController = targetViewController
    DSNetworkError.shared.body = body
    DSLog("DSService request body: ",JSON(body))
    DSNetworkError.shared.retry = 1
    DSNetworkError.shared.completionHandler = completionHandler
    DispatchQueue.main.async {
       DSNetworkError.shared.isHidden = true
    }
     let isNetworkAvailable = self.checkNetworkError()
    if isNetworkAvailable {
      Manager.request(baseUrl, method:.post, parameters:body, encoding: JSONEncoding.default).responseJSON {
        (response) in
        DSLog("response ",response.result.value ?? response)
        if response.result.value != nil {
          let responseObject = JSON(response.result.value!)
          switch response.result {
          case .success:
            completionHandler(responseObject, response.result.error)
            break
          case .failure(let error):
            DSLog("error   ",error)
            self.loadError()
            break
          }
        }else{
          self.loadError()
        }
      }
    }else{
      DSNetworkError.shared.loadErrorData(errorType: 0)
    }
  }
  
  
  func updateToken(_ json:JSON,_ targetViewController: UIViewController){
   
  }
  
  func loadError(){
    DispatchQueue.main.async {
      UIApplication.shared.keyWindow!.bringSubview(toFront: DSNetworkError.shared)
      DSNetworkError.shared.retry = 0
      DSNetworkError.shared.loadErrorData(errorType: 2)
      DSNetworkError.shared.isHidden = false
    }
  }
  
  func checkNetworkError() -> Bool {
    let isNetworkAvailable = DSNetworkManager.shared.isNetworkAvailable()
    DispatchQueue.main.async {
      if !isNetworkAvailable {
        DSNetworkError.shared.loadErrorData(errorType: 0)
        DispatchQueue.main.async {
          DSNetworkError.shared.isHidden = false
        }
      }else{
        DispatchQueue.main.async {
          DSNetworkError.shared.isHidden = true
        }
      }
    }
    return isNetworkAvailable
  }
}

