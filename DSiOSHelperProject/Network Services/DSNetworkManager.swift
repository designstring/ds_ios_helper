//
//  DSNetworkManager.swift
//  Lemoney
//
//  Created by Punith B M on 19/10/17.
//  Copyright © 2017 Lemoney. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation

public protocol NetworkStatusListener : class {
  func networkStatusDidChange(status: Reachability.Connection)
}

class DSNetworkManager: NSObject {
  
  static let shared = DSNetworkManager()  
  var listeners = [NetworkStatusListener]()
  let reachability = Reachability()!
  
  @objc func reachabilityChanged(note: Notification) {
    let reachability = note.object as! Reachability
    switch reachability.connection {
    case .wifi:
      DSLog("Reachable via WiFi")
    case .cellular:
      DSLog("Reachable via Cellular")
    case .none:
      DSLog("Network not reachable")
    }
    self.listeners.forEach{(listener) in
     listener.networkStatusDidChange(status: reachability.connection)
    }
  }
  
  func isNetworkAvailable() -> Bool {
    return reachability.connection != .none
  }
  
  func addListener(_ listener: NetworkStatusListener){
    self.removeListener(listener)
    self.listeners.append(listener)
  }
  
  func removeListener(_ listener: NetworkStatusListener){
    self.listeners =  self.listeners.filter{ $0 !== listener}
  }
  
  /// Starts monitoring the network availability status
  func startMonitoring() {
    NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
    do{
      try reachability.startNotifier()
    }catch{
      DSLog("could not start reachability notifier")
    }
  }
  
  /// Stops monitoring the network availability status
  func stopMonitoring(){
    reachability.stopNotifier()
    NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
  }
}




public var Manager: Alamofire.SessionManager = {
  var serverTrustPolicies: [String: ServerTrustPolicy] = [
    baseUrl.domain(): ServerTrustPolicy.disableEvaluation
  ]
  let configuration = URLSessionConfiguration.default
  configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
  let manager = Alamofire.SessionManager(
    configuration: configuration,
    serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
  )
  manager.session.configuration.timeoutIntervalForRequest = TimeInterval(DSPrefManager.shared.getTimeoutValue())
  return manager
}()

