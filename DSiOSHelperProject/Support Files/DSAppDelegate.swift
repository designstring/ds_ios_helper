//
//  AppDelegate.swift
//  DSiOSHelperProject
//
//  Created by Punith B M on 02/05/18.
//  Copyright © 2018 Lemoney. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import CoreLocation
import Fabric
import Crashlytics
import Foundation
import IQKeyboardManagerSwift
import LocalAuthentication
import DeviceCheck

protocol DSAppDelegateDelegate {
  func didReceiveRemoteNotification(withDetails: [AnyHashable: Any])
  func didFailToRegisterForRemoteNotificationsWithError()
  func didRegisterForRemoteNotificationsWithFCMToken(withFCMtoken: String)
  func didRegisterForRemoteNotificationsWithDeviceToken()
  func didRegisterForLocationServices(withLatLng: String, andCity: String)
  func didFailToRegisterForLocationServicesWithPermissionDenied()
}


@UIApplicationMain
class DSAppDelegate: UIResponder, MessagingDelegate, CLLocationManagerDelegate, UIApplicationDelegate {
  let application = UIApplication.shared
  let locationManager = CLLocationManager()
  var DSAppDelegate: DSAppDelegateDelegate?
  var window: UIWindow?
  var badgeCount = 0


  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//    Fabric.with([Crashlytics.self])
    IQKeyboardManager.shared.enable = true
    IQKeyboardManager.shared.enableAutoToolbar = false
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    self.locationManager.delegate = self
    return true
  }

  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }

  func registerForNotificationServices() {
    application.delegate = self
    if #available(iOS 10.0, *) {
      // For iOS 10 display notification (sent via APNS)
      UNUserNotificationCenter.current().delegate = self
      #if DEBUG
        DSLog("its coming here if condition ")
      #endif
      let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
      UNUserNotificationCenter.current().requestAuthorization(
        options: authOptions,
        completionHandler: {granted, error in
          #if DEBUG
            DSLog("completion handler coming here")
          #endif
          if (error == nil && granted) {
            self.DSAppDelegate?.didRegisterForRemoteNotificationsWithDeviceToken()
          } else {
            self.DSAppDelegate?.didFailToRegisterForRemoteNotificationsWithError()
          }
      })
    } else {
      DSLog("its coming here else condition ")
      let settings: UIUserNotificationSettings =
        UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
      application.registerUserNotificationSettings(settings)
    }
    application.registerForRemoteNotifications()
    if(FirebaseApp.app() == nil) {
      FirebaseApp.configure()
    }
    Messaging.messaging().delegate = self
  }
  
  func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
    DSLog("MessagingDelegate Firebase registration token: \(fcmToken)")
  }
  func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
    DSLog("Received data message: \(remoteMessage.appData)")
  }
  
  func application(received remoteMessage: MessagingRemoteMessage) {
    DSLog("remoteMessage: ", remoteMessage.appData)
  }
  
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
    DSLog("print notiication ",userInfo)
    badgeCount += 1
    application.applicationIconBadgeNumber = badgeCount
    DSAppDelegate?.didReceiveRemoteNotification(withDetails: userInfo)
  }
  
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
  
    DSLog("did recieve notification ",currentTopViewController())
  
    DSAppDelegate?.didReceiveRemoteNotification(withDetails: userInfo)
    
    switch application.applicationState {
    case .active:
      //app is currently active, can update badges count here
      DSLog("The app is Active")
      break
    case .inactive:
      //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
      DSLog("The app is In-Active")
      
      break
    case .background:
      //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
      DSLog("The app is in background")
      break
    }
  }
  
  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    #if DEBUG
      DSLog("Unable to register for remote notifications: \(error.localizedDescription)")
    #endif
    DSAppDelegate?.didFailToRegisterForRemoteNotificationsWithError()
  }
  
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    #if DEBUG
      DSLog("APNs token retrieved DS app delgate: \(deviceToken)")
    #endif
    Messaging.messaging().apnsToken = deviceToken
    let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
    DSPrefManager.shared.setAPNS(token:token)
  }
  
  func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
    DSPrefManager.shared.setFcmToken(token: fcmToken)
    DSAppDelegate?.didRegisterForRemoteNotificationsWithFCMToken(withFCMtoken: fcmToken)
  }
  
  func registerForLocationServices() {
    DSLog("register for location services -- func called")
    switch(CLLocationManager.authorizationStatus()) {
    case .authorizedWhenInUse, .authorizedAlways:
      self.locationManager.startUpdatingLocation()
      if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
        if CLLocationManager.isRangingAvailable() {
          if self.locationManager.location != nil {
            self.setLocation(withManager:self.locationManager, andCoordinate:self.locationManager.location!.coordinate )
            return
          }
        }
      }
      self.locationManager.stopUpdatingLocation()
    case .denied:
      DSAppDelegate?.didFailToRegisterForLocationServicesWithPermissionDenied()
      DSLog("CLLocationManager denied")
    case .restricted:
      DSAppDelegate?.didFailToRegisterForLocationServicesWithPermissionDenied()
      DSLog("CLLocationManager restricted")
    case .notDetermined:
      self.locationManager.requestAlwaysAuthorization()
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    DSLog("didChangeAuthorization for location services -- func called",status)
    if (status == .authorizedWhenInUse || status == .authorizedAlways ) {
      if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
        if CLLocationManager.isRangingAvailable() {
          if manager.location != nil {
            self.setLocation(withManager:manager, andCoordinate:manager.location!.coordinate )
            return
          }
        }
      }
    }else if (status != .notDetermined){
      DSAppDelegate?.didFailToRegisterForLocationServicesWithPermissionDenied()
    }
  }
  
  func setLocation(withManager: CLLocationManager, andCoordinate: CLLocationCoordinate2D) {
    let latLng = "\(andCoordinate.latitude),\(andCoordinate.longitude)"
//    DSPrefManager.shared.setGeoCode(code:latLng )
    if withManager.location != nil {
      CLGeocoder().reverseGeocodeLocation(withManager.location!, completionHandler: {(placemarks, error) -> Void in
        var city = ""
        if (error == nil) {
          city = placemarks?.first?.locality ?? "Bangalore"
//          DSPrefManager.shared.setLocation(withCity: city)
        }
        self.DSAppDelegate?.didRegisterForLocationServices(withLatLng: latLng, andCity: city)
      })
    }
  }
}

@available(iOS 10, *)
extension DSAppDelegate : UNUserNotificationCenterDelegate {
  // Receive displayed notifications for iOS 10 devices.
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
                              withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let userInfo = notification.request.content.userInfo
     DSLog(userInfo)
    completionHandler([])
  }
  
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
    let userInfo = response.notification.request.content.userInfo
     DSLog("userNotificationCenter ",userInfo)
    completionHandler()
  }
}

extension UIApplication {
  var statusBarView: UIView? {
    return value(forKey: "statusBar") as? UIView
  }
}
